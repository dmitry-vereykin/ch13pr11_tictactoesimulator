/**
 * Created by Dmitry on 7/28/2015.
 */
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.Random;

public class TicTacToe extends JFrame {
    private TopBanner topBanner;
    private JPanel buttonPanel;
    private JButton button;
    private JMenuBar menuBar;
    private JMenu fileMenu;
    private JMenuItem exitItem;
    private JPanel mainPanel;
    private JLabel[][] labels;

    public TicTacToe() {
        this.setTitle("Tic Tac Toe Simulator");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(new BorderLayout());

        buildMenuBar();
        buildButtonPanel();
        buildMainPanel();
        this.setJMenuBar(menuBar);

        topBanner = new TopBanner();

        this.add(topBanner, BorderLayout.NORTH);
        this.add(new JLabel("          "), BorderLayout.WEST);
        this.add(new JLabel("          "), BorderLayout.EAST);
        this.add(mainPanel, BorderLayout.CENTER);
        this.add(buttonPanel, BorderLayout.SOUTH);

        this.pack();
        this.setVisible(true);
        setLocationRelativeTo(null);
    }

    private void buildMenuBar() {
        menuBar = new JMenuBar();
        buildFileMenu();
        menuBar.add(fileMenu);
    }

    private void buildFileMenu() {
        fileMenu = new JMenu("File");
        fileMenu.setMnemonic(KeyEvent.VK_F);

        exitItem = new JMenuItem("Exit");
        exitItem.setMnemonic(KeyEvent.VK_X);
        exitItem.addActionListener(new ExitListener());

        fileMenu.add(exitItem);
    }

    private void buildMainPanel(){
        mainPanel = new JPanel(new GridLayout(3, 3));

        labels = new JLabel[3][3];

        for (int j = 0; j < 3; j++) {
            for (int i = 0; i < 3; i++) {
                labels[i][j] = new JLabel(" ");
                labels[i][j].setFont(new Font("Arial", Font.PLAIN, 96));
                mainPanel.add(labels[i][j]);
            }
        }
    }

    private void buildButtonPanel() {
        buttonPanel = new JPanel();
        button = new JButton("New Game");
        button.setMnemonic(KeyEvent.VK_G);
        button.addActionListener(new NewGameButtonListener());
        buttonPanel.add(button);
    }

    private class NewGameButtonListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            String[][] board = new String[3][3];
            Random random = new Random();
            int who = random.nextInt(2);
            String firstPlayer;
            String secondPlayer;
            int i;
            int j;

            if (who == 1) {
                firstPlayer = "X";
                secondPlayer = "O";
            } else {
                firstPlayer = "O";
                secondPlayer = "X";
            }

            for (int z = 0; z < 3; z++) {
                for (int s = 0; s < 3; s++) {
                    board[s][z] = "";
                }
            }

            for (int turn = 0; turn < 9; turn++) {
                if (turn % 2 == 0) {
                    while (true) {
                        i = random.nextInt(3);
                        j = random.nextInt(3);


                        if (!board[i][j].equals("X") && !board[i][j].equals("O")) {
                            board[i][j] = firstPlayer;
                            labels[i][j].setText(firstPlayer);
                            break;
                        }
                    }
                } else {
                    while (true) {
                        i = random.nextInt(3);
                        j = random.nextInt(3);

                        if (!board[i][j].equals("X") && !board[i][j].equals("O")) {
                            board[i][j] = secondPlayer;
                            labels[i][j].setText(secondPlayer);
                            break;

                        }
                    }
                }

            }


            pack();


            if ((board[0][0].equals("X") && board[0][1].equals("X") && board[0][2].equals("X") || // 1st row
                    board[1][0].equals("X") && board[1][1].equals("X") && board[1][2].equals("X") || // 2nd row
                    board[2][0].equals("X") && board[2][1].equals("X") && board[2][2].equals("X") || // 3rd row
                    board[0][0].equals("X") && board[1][0].equals("X") && board[2][0].equals("X") || // 1st col
                    board[0][1].equals("X") && board[1][1].equals("X") && board[2][1].equals("X") || // 2nd col
                    board[0][2].equals("X") && board[1][2].equals("X") && board[2][2].equals("X") || // 3rd col
                    board[0][0].equals("X") && board[1][1].equals("X") && board[2][2].equals("X") || // Diagonal left
                    board[2][0].equals("X") && board[1][1].equals("X") && board[0][2].equals("X")) &&
                    (board[0][0].equals("O") && board[0][1].equals("O") && board[0][2].equals("O") || // 1st row
                    board[1][0].equals("O") && board[1][1].equals("O") && board[1][2].equals("O") || // 2nd row
                    board[2][0].equals("O") && board[2][1].equals("O") && board[2][2].equals("O") || // 3rd row
                    board[0][0].equals("O") && board[1][0].equals("O") && board[2][0].equals("O") || // 1st col
                    board[0][1].equals("O") && board[1][1].equals("O") && board[2][1].equals("O") || // 2nd col
                    board[0][2].equals("O") && board[1][2].equals("O") && board[2][2].equals("O") || // 3rd col
                    board[0][0].equals("O") && board[1][1].equals("O") && board[2][2].equals("O") || // Diagonal left
                    board[2][0].equals("O") && board[1][1].equals("O") && board[0][2].equals("O"))) {
                JOptionPane.showMessageDialog(null, "Friendship :D");
            } else if (board[0][0].equals("X") && board[0][1].equals("X") && board[0][2].equals("X") || // 1st row
                    board[1][0].equals("X") && board[1][1].equals("X") && board[1][2].equals("X") || // 2nd row
                    board[2][0].equals("X") && board[2][1].equals("X") && board[2][2].equals("X") || // 3rd row
                    board[0][0].equals("X") && board[1][0].equals("X") && board[2][0].equals("X") || // 1st col
                    board[0][1].equals("X") && board[1][1].equals("X") && board[2][1].equals("X") || // 2nd col
                    board[0][2].equals("X") && board[1][2].equals("X") && board[2][2].equals("X") || // 3rd col
                    board[0][0].equals("X") && board[1][1].equals("X") && board[2][2].equals("X") || // Diagonal left
                    board[2][0].equals("X") && board[1][1].equals("X") && board[0][2].equals("X")) { // Diagonal right
                JOptionPane.showMessageDialog(null, "X Wins!");
            } else if (board[0][0].equals("O") && board[0][1].equals("O") && board[0][2].equals("O") || // 1st row
                    board[1][0].equals("O") && board[1][1].equals("O") && board[1][2].equals("O") || // 2nd row
                    board[2][0].equals("O") && board[2][1].equals("O") && board[2][2].equals("O") || // 3rd row
                    board[0][0].equals("O") && board[1][0].equals("O") && board[2][0].equals("O") || // 1st col
                    board[0][1].equals("O") && board[1][1].equals("O") && board[2][1].equals("O") || // 2nd col
                    board[0][2].equals("O") && board[1][2].equals("O") && board[2][2].equals("O") || // 3rd col
                    board[0][0].equals("O") && board[1][1].equals("O") && board[2][2].equals("O") || // Diagonal left
                    board[2][0].equals("O") && board[1][1].equals("O") && board[0][2].equals("O")) { // Diagonal right
                JOptionPane.showMessageDialog(null, "O Wins!");
            } else {
                JOptionPane.showMessageDialog(null, "Tie!!");
            }

        }
    }

    private class ExitListener implements ActionListener {
        public void actionPerformed(ActionEvent e){
            System.exit(0);
        }
    }


    class TopBanner extends JPanel {
        private JLabel topBanner;

        public TopBanner() {
            topBanner = new JLabel("----------------Press \"New Game\" button----------------");
            add(topBanner);
        }
    }

    public static void main(String[] args) {
        new TicTacToe();
    }
}
